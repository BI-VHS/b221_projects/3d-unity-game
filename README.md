# 3D Unity Game

3D detektivní hra z pohledu první osoby. Ve hře je implementovaný systém, který umožní dynamicky upravovat příběh hry, což umožňuje opakovné hraní.

## Příběh
Příběh začíná v jednom malém pražském bytě uprostřed noci zvoněním telefonu. V ten moment se 
hratelná postava detektiva pražské kriminálky Jiřího Loudy probouzí a bere telefon. V telefonu se 
ozve jeho kolega Vašek se slovy „Jirko, hoď na sebe kabát a do 15 minut ať jsi v parku na 
Vinohradský. Máme práci.“ Jiří tedy musí v bytě najít telefon, zbraň a kabát a vyrazit. Hledáním ho 
provede stručný tutoriál. Po opuštění bytu se objeví v parku.

V parku mu policista vedle řekne, že oběť byla nalezena až za kašnou mezi stromy a že už tam na něj 
kolega Václav čeká. Jakmile hráč projde parkem a najde místo činu, proběhne krátký dialog s 
Václavem, který mu řekne, že je tu jedna očitá svědkyně, která čeká u policejního vozu a je lehce v šoku. 
Nyní má hráč za úkol prohledat park a najít důkazy. Dále si promluvit se svědkyní. Jakmile si bude 
myslet, že má vše, může se pomocí interakce u policejního auta přesunout na služebnu. Ze služebny 
se může naopak pomocí dveří vrátit do parku.

Na služebně ve své kanceláři má whiteboard, kde se postupně objevují důkazy a důležitá tvrzení 
svědků. Dále je na služebně výslechová místnost, kam si může hráč zvát příbuzné/známé oběti. 
Hráčův úkol je samozřejmě odhalit správného pachatele. Pokud se mu to povede, pachatel je vzat do 
vazby. Pokud se mu to nepovede a obviní někoho jiného, pachateli se povede utéct.
Příběh nelze pevně definovat, pro větší zajímavost jsou zde možné 4 různé konce (pachatelé), podle 
kterých se příběh od scény z parku odvíjí. Viz. dále.

## Dílčí dokumentace
[Charaktery popis](Documentation/characters.md)

[Statický svět](Documentation/static_world.md)

[Dynamický svět](Documentation/dynamic_world.md)

[Kompletní svět](Documentation/complete_world.md)
