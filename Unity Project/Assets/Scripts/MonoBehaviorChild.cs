using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoBehaviourChild : MonoBehaviour
{
    public bool lastInteractedObject = false;

    private bool interactable = false;

    public void SetInteractable(bool newState) {
        interactable = newState;
    }

    public bool GetInteractable() {
        return interactable;
    }

    public void toggleInteractionMessage(int message) {
          if(GetInteractable()) {
              // pick up item
              GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(message);
              this.lastInteractedObject = true;
          } else if(this.lastInteractedObject && GameObject.Find("InteractionText").GetComponent<InteractionMessage>().GetMessageType() == message) {
              // clear message
              GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(-1);
              this.lastInteractedObject = false;
          }
    }
}
