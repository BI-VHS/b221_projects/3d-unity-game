using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameState : MonoBehaviour
{

    private TextMeshProUGUI gameStateText;

    private Dictionary<string, int> spawningPoints;

    private int state;

    // Start is called before the first frame update
    void Start()
    {
        spawningPoints = new Dictionary<string, int>();
        this.gameStateText = GameObject.Find("GameStateText").GetComponent<TextMeshProUGUI>();
        
        SetState(901);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetState(int newState) {

        if(newState == 902) {
            Debug.Log(2);
            this.state = 0;
            newState = 0;
        }


        if(this.state != -1 && this.state != 1 && this.state != 901 && this.gameStateText is not null) {
            switch(newState) {
                case -1:
                    this.gameStateText.text = "Tohle není Sazka, ale policie. Nemůžeš jentak tipovat. Konec hry...";
                    break;
                case 0:
                    this.gameStateText.text = "";
                    this.state = 0;
                    break;
                case 1:
                    this.gameStateText.text = "Gratuluji, našel jsi vraha!";
                    break;
                case 2:
                    this.gameStateText.text = "Sakra, ještě mi asi něco chybí!";
                    break;
                case 4:
                    if(this.state == newState) {
                        this.gameStateText.text = "Ten odznak potřebuji, kartička do fitka co mam v kapse stačit nebude.";
                    } else {
                        this.gameStateText.text = "Nemůžu odejít bez odznaku, co jsem to za policistu...";
                    }
                    break;
                case 5:
                    if(this.state == newState) {
                        this.gameStateText.text = "Už takhle mam 7 dioptrií a tma mi moc nepomáhá... ta baterka by se opravdu šikla.";
                    } else {
                        this.gameStateText.text = "Venku je tma, měl bych si vzít raději i baterku... kde jsem ji jen nechal?";
                    }
                    break;
                case 6:
                    if(this.state == newState) {
                        this.gameStateText.text = "Ne, prostě bez zbraně nemohu! Kolegové by se zase smáli.";
                    } else {
                        this.gameStateText.text = "Sakra, nemůžu zase odejít bez služební zbraně!";
                    }
                    break;
                case 3:
                    if(GameObject.Find("Inventory").GetComponent<Inventory>().IsInInventory("phone")) {
                        InventoryItem item = new InventoryItem();
                        item.id = "email";
                        item.displayName = "Email";
                        item.evidence = false;
                        GameObject.Find("Inventory").GetComponent<Inventory>().AddItem(item);
                        this.state = 0;
                    }
                    break;
                case 900:
                    this.gameStateText.text = "Loading";
                    break;
                case 901:
                    this.gameStateText.text = "V noci tě probudil telefon od kolegy. Něco se stalo v parku. Vezmi si služební zbraň, odznak a baterku a vyraž \n \n W, A, S, D - Pohyb \n E - Interakce \n F - Baterka \n Esc - Odchod z konverzace/hry \n Enter - Potvrzení konverzace \n Pro konverzaci využívej zvýrazněná klíčová slova a důkazy.\n Zavřít [Esc] ";
                    break;
            }

            this.state = newState;
        }
    }

    public int GetState() {
        return this.state;
    }

    public bool HavePosition(string itemId) {
        return spawningPoints.ContainsKey(itemId);
    }

    public int GetSpawnPoint(string itemId) {
        return spawningPoints[itemId];
    }

    public void AddPosition(string itemId, int spawnPointId) {
        this.spawningPoints.Add(itemId, spawnPointId);
    }

}
