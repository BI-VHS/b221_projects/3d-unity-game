/* using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class wallet : MonoBehaviour
{
    public bool walletOpen = false;
    [SerializeField] private Animator animCon;

    public Interactable interactable;

    public SteamVR_Action_Single pinchSqueeze = SteamVR_Input.GetAction<SteamVR_Action_Single>("Squeeze");

    private void Start()
    {
        if (animCon == null)
            animCon = GameObject.Find("wallet").GetComponent<Animator>();
        if (interactable == null)
            interactable = GameObject.Find("grabableWallet").GetComponent<Interactable>();
    }

    // Update is called once per frame
    void Update()
    {
        if(interactable.attachedToHand) {
            if(SteamVR_Actions._default.GrabPinch.GetStateDown(interactable.attachedToHand.handType)) {
                walletOpen = !walletOpen;
            }
        }
        this.animCon.SetBool("walletOpen", walletOpen);
    }
}
 */