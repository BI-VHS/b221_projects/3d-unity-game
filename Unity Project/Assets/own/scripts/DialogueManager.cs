using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using TMPro;

public class DialogueManager : MonoBehaviour
{

    private GameObject playerObject;
    private bool inDialogue = false;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dialogueText;
    public TextMeshProUGUI playerText;
    public string highlightColor = "ff0000";
    private string inDiscussWith;
    public TMP_InputField lInputField;
    public Animator animator;
    public int maxRepeatSentence = 10;
    public List<string> globalKeyWords;
    public List<Dialogue> dialogues;
    private bool startSentenceUsed = false;
    private Dictionary<string, Dialogue> dialoguesDict;
    private Dialogue dialogueInUse;

    private List<string> startBy;
    
    
    private /* Queue<Sentence> */ Sentence[] sentences;
    private string inputText;

    // Start is called before the first frame update
    void Start()
    {
        startBy = new List<string>();
        dialoguesDict = new Dictionary<string, Dialogue>();
        foreach (Dialogue dialogue in dialogues) {
            if(dialogue.startByDistance > 1.0f) {
                startBy.Add(dialogue.gameObjectName);
            }
            dialogue.PrepareKeyWords(highlightColor);
            dialoguesDict.Add(dialogue.gameObjectName, dialogue);
        }

        playerObject = GameObject.Find("PlayerArmature");
        nameText = GameObject.Find("Canvas/dialogueWrapper/nameText").GetComponent<TextMeshProUGUI>();
        dialogueText = GameObject.Find("Canvas/dialogueWrapper/dialogueText").GetComponent<TextMeshProUGUI>();
        playerText = GameObject.Find("Canvas/dialogueWrapper/playerText").GetComponent<TextMeshProUGUI>();
        lInputField = GameObject.Find("Canvas/dialogueWrapper/PlayerInputField").GetComponent<TMP_InputField>();  
    }

    void Update() {
        foreach (string start in startBy) {
            GameObject startByObject = GameObject.Find(start);
            if(startByObject is not null) {
                Transform byNear = startByObject.GetComponent<Transform>();
                if(byNear is not null) {
                    float distance = Vector3.Distance(byNear.position, playerObject.GetComponent<Transform>().position);
                    if(distance < dialoguesDict[start].startByDistance) {
                        this.SetInDiscussWith(start);
                        this.StartDialogue();
                    }
                }
            }
        }    
    }

    public void StartDialogue() {
       if(!inDialogue) {
            
            dialogueInUse = GetNPCDialog(GetInDiscussWith());
            dialogueInUse.SetAlreadyMeet(true);
            lInputField.text = "";
            
            animator.SetBool("IsOpen", true);
            inDialogue = true;

            nameText.text = dialogueInUse.name;
            Debug.Log("Starting conversation with " + dialogueInUse.name);

            sentences = dialogueInUse.sentences;
            DisplayNextSentence();
       }
    }

    public Dictionary<string, Dialogue> GetWholeDialogDictionary() {
        return dialoguesDict;
    }

    public Dialogue GetNPCDialog(string name) {
        return dialoguesDict[name];
    }

    public bool GetInDialogue() {
        return inDialogue;
    }

    public void ReadInput(TMP_InputField textInput) {
        
        inputText = textInput.text;
    }

     public void clearInput(TMP_InputField textInput) {
        textInput.text = "";
    }

    public void DisplayNextSentence() {
        if(sentences.Length == 0) {
            EndDialogue();
            return;
        }

        // read text from input
        ReadInput(lInputField);
        
        // search sentence
        openDialog(searchSentence());
        GameObject whiteBoardObj = GameObject.Find("whiteBoardText");
        if(whiteBoardObj is not null) {
            WhiteBoard whiteBoard =  whiteBoardObj.GetComponent<WhiteBoard>();
            whiteBoard.UpdateTextsDialogs();
        }
    }

    public void ResolveLogicalScript(Sentence sentence) {
        if(sentence.logicalScriptObject != "") {
            GameObject logicalScript = GameObject.Find(sentence.logicalScriptObject);
            if(logicalScript is not null) {
                logicalScript.GetComponent<GameState>().SetState(sentence.logicalState);
            } 
        }
    }

    public void PlayAnimation(int state) {
        if(this.dialogueInUse.animationObject != "" && state != 0) {
            Animator npcAnimator = GameObject.Find(this.dialogueInUse.animationObject).GetComponent<Animator>(); 
            npcAnimator.SetInteger("State", state);
        } 
    }

    public Sentence searchSentence() {
        Sentence sentence = new Sentence();
        sentence.text = "Co prosím?";
        sentence.playerText = "";
        if(!startSentenceUsed) {
            sentence = this.getSentenseTexts(0, sentence);
            startSentenceUsed = true;
        } else {
            if(inputText != "") {
                // loop for sentences
                for (int index = 0; index < sentences.Length; index++)
                {            
                    string keys = sentences[index].keys;
                    string[] keysArray = keys.Split(';');

                    foreach (string word in keysArray)
                    {
                        if(word.ToLower() == inputText.ToLower() && (sentences[index].neededEvidence == "" || GameObject.Find("Inventory").GetComponent<Inventory>().IsInInventory(sentences[index].neededEvidence))) {
                            if(this.dialoguesDict[GetInDiscussWith()].startByDistance > 1) {
                                this.dialoguesDict[GetInDiscussWith()].startByDistance = 0;
                                this.dialoguesDict[GetInDiscussWith()].sentences[0].repeatable = false;
                            }
                            this.dialoguesDict[GetInDiscussWith()].sentences[index].useCounter++;
                            if(this.dialoguesDict[GetInDiscussWith()].sentences[index].useCounter == maxRepeatSentence) {
                                this.dialoguesDict[GetInDiscussWith()].sentences[index].repeatable = false;
                            }
                            sentence = this.getSentenseTexts(index, sentence);
                            
                        }
                    }
                }
            }
        }
        return sentence;
    }

    private Sentence getSentenseTexts(int index, Sentence sentence) {
        if(sentences[index].used && !sentences[index].repeatable) {
            sentence.text = (sentences[index].usedText != "" ? sentences[index].usedText : "To snad už víte!");
            sentence.playerText = sentences[index].playerText;
            sentence.animationState = sentences[index].animationState;
        } else {
            sentences[index].used = true;
            sentence = sentences[index];
        }
        return sentence;
    }

    public void openDialog(Sentence sentence) {
        PlayAnimation(sentence.animationState);
        ResolveLogicalScript(sentence);
        clearInput(lInputField);
        lInputField.Select();
        lInputField.ActivateInputField();
        StopAllCoroutines();
        Debug.Log(sentence.text);
        StartCoroutine(TypeSentence(playerText, sentence.playerText));
        StartCoroutine(TypeSentence(dialogueText, sentence.text));
    }

    IEnumerator TypeSentence(TextMeshProUGUI guiObject, string sentenceText) {
        
        guiObject.text = "";
        foreach(char letter in sentenceText.ToCharArray()) {
            guiObject.text += letter;
            yield return null;
        }
    }

    public void EndDialogue() {
        /* playerObject.GetComponent<StarterAssetsInputs>().typing = false; */
        this.startSentenceUsed = false;
        lInputField.text = "";
        this.inDialogue = false;
        animator.SetBool("IsOpen", false);
    }

    public string GetInDiscussWith() {
        return inDiscussWith;
    }

    public void SetInDiscussWith(string inDiscussWithNew) {
        inDiscussWith = inDiscussWithNew;
    }
}
