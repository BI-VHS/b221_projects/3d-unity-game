using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct InventoryItem
{
    public string id;
    public string displayName;
    public bool evidence;
}
