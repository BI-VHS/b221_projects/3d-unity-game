using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : InteractionObjects
{

    public GameObject doorAnimationObject;
    public bool open = false;
    public AudioClip[] sfx;

    // Start is called before the first frame update
    void Start()
    {
        doorAnimationObject.GetComponent<Animator>().SetBool("open", open);
    }

    public override void OnInteract()
    {
        if (sfx.Length > 1)
            AudioSource.PlayClipAtPoint(sfx[open? 1 : 0], transform.position, 10.0f);
        open = !open;
        doorAnimationObject.GetComponent<Animator>().SetBool("open", open);
    }

    void Update() {
        toggleInteractionMessage(6);
    }
}
