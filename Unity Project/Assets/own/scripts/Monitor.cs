using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Monitor : MonoBehaviourChild
{
    public Material OFFMaterial;
    public Material recievedEmailMaterial;
    private bool recievedEmail = false;
    private MeshRenderer renderer;
    private Material[] materials;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GameObject.Find("display").GetComponent<MeshRenderer>();
        ChangeLampMaterial();
    }

    // Update is called once per frame
    void Update()
    {
        if(!recievedEmail) {
            toggleInteractionMessage(12);

            if(GameObject.Find("Inventory").GetComponent<Inventory>().IsInInventory("email")) {
                this.recievedEmail = true;
                ChangeLampMaterial();
            }
        }
    }

    private void ChangeLampMaterial() {
        materials = renderer.materials;
        materials[1] = (this.recievedEmail ? recievedEmailMaterial : OFFMaterial);
        renderer.materials = materials;
    }
}
