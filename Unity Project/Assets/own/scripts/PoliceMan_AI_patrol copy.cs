/* using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PoliceMan_AI_patrol : MonoBehaviour
{

    private  Animator animator;
    public Transform[] points;
    private int destPoint = 0;
    private NavMeshAgent agent;
    private bool searching = true;
    private float distance = 20.0f;


    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        this.animator = GetComponent<Animator>();
        agent.autoBraking = false;
        GotoNextPoint();
    }

    // Update is called once per frame
    void Update()
    {
        if (!agent.pathPending && agent.remainingDistance < distance) {
        
            if(!searching) {
                this.animator.SetInteger("state", 1);
            }

            if(this.animator.GetCurrentAnimatorStateInfo(0).IsName("BasicMotions@Walk01")) {
                GotoNextPoint();
            }
        } else {
            searching = false;
        } 
    }

    void GotoNextPoint() {
        agent.enabled = true;
        // Returns if no points have been set up
        if (points.Length == 0) {
            return;
        }
        
        // Set the agent to go to the currently selected destination.
        agent.destination = points[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % points.Length;
    }
}
 */