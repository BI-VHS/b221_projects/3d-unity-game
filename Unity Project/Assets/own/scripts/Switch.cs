using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : InteractionObjects
{

    private GameObject playerObject;
    public GameObject light;
    public bool isOn;
    public float onIntensity;
    public float interactiveDistance = 60f;

    public AudioClip[] sfx;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("START");
        playerObject = GameObject.Find("PlayerArmature");

        if(isOn) {
            light.GetComponent<Light>().intensity = onIntensity;
        } else {
            light.GetComponent<Light>().intensity = 0;
        }
    }

    public override void OnInteract()
    {
        if(isOn) {
            isOn = false;
            light.GetComponent<Light>().intensity = 0;

            if (sfx.Length > 0)
                AudioSource.PlayClipAtPoint(sfx[0], transform.position);

        } else {
            isOn = true;
            light.GetComponent<Light>().intensity = onIntensity;

            if (sfx.Length > 1)
                AudioSource.PlayClipAtPoint(sfx[1], transform.position);;

        }
    }

    void Update() {
        toggleInteractionMessage(8);
    }

}
