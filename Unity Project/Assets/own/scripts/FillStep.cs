using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillStep : InteractionObjects
{

    public GameObject stepFill;
    private bool fill;

    // Start is called before the first frame update
    void Start()
    {
        stepFill = GameObject.Find("stepFill");
    }

    public override void OnInteract()
    {
        if(!fill) {
            fill = true;
            stepFill.GetComponent<Animator>().SetBool("Fill", fill);
        }
    }

    void Update() {
        if(GetInteractable()) {
            if(!fill) {
                // pick up item
                GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(11);
            }
            lastInteractedObject = true;
        } else if(lastInteractedObject && GameObject.Find("InteractionText").GetComponent<InteractionMessage>().GetMessageType() == 11) {
            // clear message
            GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(-1);

            lastInteractedObject = false;
        }
    }

}
