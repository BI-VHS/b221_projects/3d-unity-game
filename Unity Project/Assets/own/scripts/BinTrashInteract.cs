using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BinTrashInteract : InteractionObjects
{
    private bool remove = false;
    public AudioClip[] sfx;

    public override void OnInteract()
    {
        if (sfx.Length > 1)
            AudioSource.PlayClipAtPoint(sfx[remove? 1 : 0], transform.position, 10.0f);
        remove = !remove;
        this.gameObject.GetComponent<Animator>().SetBool("remove", remove);

    }

    void Update() {
        if(GetInteractable()) {
            if(!remove) {
                // pick up item
                GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(9);
            } else {
                // pick up item
                GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(10);
            }
            lastInteractedObject = true;
        } else if(lastInteractedObject && (GameObject.Find("InteractionText").GetComponent<InteractionMessage>().GetMessageType() == 9 || GameObject.Find("InteractionText").GetComponent<InteractionMessage>().GetMessageType() == 10)) {
            // clear message
            GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(-1);
            lastInteractedObject = false;
        }
    }
}
