using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class Inventory : MonoBehaviour
{
    private List<InventoryItem> items;
    private TextMeshProUGUI ui;

    // Start is called before the first frame update
    void Start()
    {
        items = new List<InventoryItem>();
        ui = GameObject.Find("Canvas/Text").GetComponent<TextMeshProUGUI>();
        ui.text = "Nalezené objekty: \n";
    }

    public void AddItem(InventoryItem item)
    {
        items.Add(item);
        ui.text += item.displayName + '\n';
        GameObject whiteBoardObj = GameObject.Find("whiteBoardText");
        if(whiteBoardObj is not null) {
            WhiteBoard whiteBoard = whiteBoardObj.GetComponent<WhiteBoard>();
            whiteBoard.UpdateTextsDialogs();
        }
    }

    public List<InventoryItem> GetItems()
    {
        return items;
    }

    public bool IsInInventory(string id)
    {
        bool state = false;
        foreach(InventoryItem item in items.ToArray()) {
            if(item.id == id) {
                state = true;
            }
        }
        return state;
    }
}
