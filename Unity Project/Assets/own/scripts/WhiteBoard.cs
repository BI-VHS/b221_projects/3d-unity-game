using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WhiteBoard : MonoBehaviour
{
    private DialogueManager dialogueManager;
    private Inventory inventory;

    private TextMeshPro evidenceTable;
    private TextMeshPro dialogueTable;

    void Start() {

        // get dialogueManager and inventory objects
        dialogueManager = FindObjectOfType<DialogueManager>();
        inventory = FindObjectOfType<Inventory>();

        
        if( GameObject.Find("EvidenceTable") is not null && GameObject.Find("dialogueTable") is not null) {
            evidenceTable = GameObject.Find("EvidenceTable").GetComponent<TextMeshPro>();
            dialogueTable = GameObject.Find("dialogueTable").GetComponent<TextMeshPro>();

            UpdateTextsEvidence();
            UpdateTextsDialogs();
        }
    }

    public void UpdateTextsEvidence() {
        
        if( GameObject.Find("EvidenceTable") is not null) {
            evidenceTable.text = "Důkazy: \n";
            if( FindObjectOfType<Inventory>().GetItems() is not null ) {
                if(inventory.GetItems().Count > 0) {
                    
                    // add all evidences to the table
                    foreach( InventoryItem item in inventory.GetItems() )
                    {   
                        if(item.evidence) {
                            evidenceTable.text += " - " + item.displayName + "\n";
                        } 
                    }
                }
            }
        }
    }

    public void UpdateTextsDialogs() {
        
        if( GameObject.Find("dialogueTable") is not null) {
            dialogueTable.text = "Komunikace: \n";
            // add all 
            if( dialogueManager.GetWholeDialogDictionary() is not null ) {
                foreach( KeyValuePair<string, Dialogue> dialogue in dialogueManager.GetWholeDialogDictionary() )
                {
                    if(dialogue.Value.relevant && dialogue.Value.GetAlreadyMeet()) {
                        dialogueTable.text += "<color=#336600>" + dialogue.Value.name + "</color>\n";
                        foreach( Sentence sentence in dialogue.Value.sentences) {
                            if(sentence.used) {
                                dialogueTable.text += "   - " + sentence.text + "\n";
                            }
                        }
                    }
                    
                }
            }
        }
    }
}
