using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PoliceMan_AI_patrol : MonoBehaviour
{

    private  Animator animator;
    public Transform[] points;
    private int destPoint = 0;
    private NavMeshAgent agent;
    private bool searching = true;
    private float distance = 10.0f;
    private bool start = false;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!start) {
            agent = GetComponent<NavMeshAgent>();
            this.animator = GetComponent<Animator>();
            agent.autoBraking = false;
            start = true;
            
        }
        StartCoroutine(StateAnimation());            
    }

    void GotoNextPoint() {
        
        this.animator.SetInteger("state", 0); 
        // Returns if no points have been set up
        if (points.Length == 0) {
            return;
        }
        
        // Set the agent to go to the currently selected destination.
        agent.destination = points[destPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % points.Length;
    }

    void Search() {
        searching = true;
        if(searching) {
            this.animator.SetInteger("state", 1);
            GameObject.Find("SpotlightFlashLightGuard").GetComponent<Light>().enabled = true;
        }
    }


    IEnumerator StateAnimation() {
        agent.enabled = true;
        if (!agent.pathPending && agent.remainingDistance < distance) {
            agent.enabled = false;   
            Search();
            yield return new WaitForSeconds(10);
            agent.enabled = true; 
            searching = false;
            GameObject.Find("SpotlightFlashLightGuard").GetComponent<Light>().enabled = false; 
            if(!searching) {
                GotoNextPoint();
                Debug.Log("NExt");
            }
            yield return new WaitForSeconds(30);
        }
    }
}
