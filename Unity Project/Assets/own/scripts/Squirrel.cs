using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Squirrel : MonoBehaviour
{

    private  Animator animator;
    private NavMeshAgent agent;
    private GameObject[] grabbableItems;
    private bool onTheWay = false;
    private int destPoint = 0;
    private int animationState = 0;
    public GameObject nutt;
    private float timeDelay = 20;
    private bool createdNutt = false;



    // Start is called before the first frame update
    void Start()
    {
        this.agent = GetComponent<NavMeshAgent>();
        this.animator = GetComponent<Animator>();
        this.grabbableItems =  GameObject.FindGameObjectsWithTag("squirrelPoint");
        Debug.Log(grabbableItems[0]);
    }

    void GoTo() {
        onTheWay = true;
    }

     // Update is called once per frame
    void Update()
    {
        if(!this.onTheWay) {
            StartCoroutine(StateAnimation());
        } else {
            if (!agent.pathPending && agent.remainingDistance < 30.0f) {
                
                CreateNutt();
                this.agent.enabled = false;

                this.onTheWay = false;
            }
        }
    }

    void GotoNextPoint() {
        // Returns if no points have been set up
        if (grabbableItems.Length == 0) {
            return;
        }

        this.agent.enabled = true;
        
        // Set the agent to go to the currently selected destination.
        this.agent.destination = grabbableItems[destPoint].GetComponent<Transform>().position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        destPoint = (destPoint + 1) % grabbableItems.Length;
        if (agent.pathPending && agent.remainingDistance > 30.0f) {
            createdNutt = false;
        }
    }

    void CreateNutt() {
        if(!createdNutt) {
            createdNutt = true;
            GameObject nuttNew = GameObject.Instantiate(nutt);
            nuttNew.transform.position = GetComponent<Transform>().position;
        }
    }

    IEnumerator StateAnimation() {
        
        if(!this.onTheWay) {
            // float timeDelay = Random.Range(1.0f, 20.0f);
            if(this.animationState == 3 ) {
                this.animationState = 0;
            }
            
            this.animationState++;

            this.animator.SetInteger("state", this.animationState);
            yield return new WaitForSeconds(this.timeDelay);
            if(this.animationState == 2) {
                this.onTheWay = true;
                GotoNextPoint();
            }
        }
    }
}
