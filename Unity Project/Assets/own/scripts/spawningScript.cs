using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;
using System.IO;
using System;
using System.Linq;

public class spawningScript : MonoBehaviour
{
    private InteractionItem[] grabbableItems;
    private SpawnPoints[] spawningPoints;
    private Random rnd = new Random();
    private GameState gameState;
    private Inventory inventory;
    private bool spawned = false;


    // Start is called before the first frame update
    void Start()
    {   
        
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!spawned) {
            this.spawned = true;
            SpawnObjects();
        }
    }

    private void SpawnObjects() {
        this.grabbableItems = GameObject.FindObjectsOfType<InteractionItem>();
        this.spawningPoints = GameObject.FindObjectsOfType<SpawnPoints>();
        List<SpawnPoints> spawningPointsList = new List<SpawnPoints>(spawningPoints);
        Debug.Log("Tak co?");
        foreach (InteractionItem grabbableItem in grabbableItems) 
        {
            GameState gameState = GameObject.Find("GameStateText").GetComponent<GameState>();
            Inventory inventory = GameObject.Find("Inventory").GetComponent<Inventory>(); 
             
            if(gameState.HavePosition(grabbableItem.GetItemId()) && !inventory.IsInInventory(grabbableItem.GetItemId())) {
                SpawnPoints spawnPoint = SearchPosition(gameState.GetSpawnPoint(grabbableItem.GetItemId()));
                if(spawnPoint is not null) {
                    GoToPosition(grabbableItem, spawnPoint);
                }
            } else if (inventory.IsInInventory(grabbableItem.GetItemId())) {
                grabbableItem.GetComponent<Transform>().position =  GameObject.Find("trashPoint").GetComponent<Transform>().position;
            } else {
                int spawnPointIndex = rnd.Next(0,spawningPointsList.Count);
                
                SpawnPoints spawnPoint = spawningPointsList.ElementAt(spawnPointIndex);
                Debug.Log("ADD???");

                gameState.AddPosition(grabbableItem.GetItemId(), spawnPoint.id);
                GoToPosition(grabbableItem, spawnPoint);
                spawningPointsList.RemoveAt(spawnPointIndex);
            }
            
        }
        this.spawned = true;
    }

    void GoToPosition (InteractionItem item, SpawnPoints spawnPoint) {
        item.GetComponent<InteractionItem>().GetItemId();
        item.GetComponent<Transform>().position = spawnPoint.GetComponent<Transform>().position;
    }

    public SpawnPoints SearchPosition(int id) {
        foreach (SpawnPoints spawnPoint in spawningPoints) 
        {
            if(spawnPoint.id == id) {
                return spawnPoint;
            }
        }
        return null;
    }
}
