using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroySelf : MonoBehaviourChild
{
    public AudioClip sfx;
    public void Destroy() {
        // clear message
        GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(-1);

        if (sfx != null)
            AudioSource.PlayClipAtPoint(sfx, transform.position, 10.0f);

        Destroy(gameObject);
    }

    void Update() {
        toggleInteractionMessage(7);
    }
}
