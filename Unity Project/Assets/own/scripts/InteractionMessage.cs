using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InteractionMessage : MonoBehaviour
{

    private TextMeshProUGUI interactionText;

    private Dictionary<string, int> spawningPoints;

    private int messageType = -1;

    // Start is called before the first frame update
    void Start()
    {
        spawningPoints = new Dictionary<string, int>();
        interactionText = GameObject.Find("InteractionText").GetComponent<TextMeshProUGUI>();
        interactionText.text = "";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetMessageType(int newType) {
        messageType = newType;
            if(this.interactionText is not null) {
                switch(messageType) {
                case -1:
                    this.interactionText.text = "";
                    break;
                case 0:
                    this.interactionText.text = "Použij [E] pro interakci.";
                    break;
                case 1:
                    this.interactionText.text = "Odejít z bytu [E]";
                    break;
                case 2:
                    this.interactionText.text = "Přesunout se na policejní stanici [E]";
                    break;
                case 3:
                    this.interactionText.text = "Vrátit se do parku [E]";
                    break;
                case 4:
                    this.interactionText.text = "Promluvit si [E]";
                    break;
                case 5:
                    this.interactionText.text = "Sebrat předmět [E]";
                    break;
                case 6:
                    this.interactionText.text = "Otevřít dveře [E]";
                    break;
                case 7:
                    this.interactionText.text = "Roztáhnout závěs [E]";
                    break;
                case 8:
                    this.interactionText.text = "Stisknout vypínač [E]";
                    break;
                case 9:
                    this.interactionText.text = "Sundat víko [E]";
                    break;
                case 10:
                    this.interactionText.text = "Nandat víko [E]";
                    break;
                case 11:
                    this.interactionText.text = "Vytvořit odlitek [E]";
                    break;
                case 12:
                    this.interactionText.text = "Přečíst email [E]";
                    break;
            }
            }
            
    }

    public int GetMessageType() {
        return messageType;
    }

    public bool HavePosition(string itemId) {
        return spawningPoints.ContainsKey(itemId);
    }

    public int GetSpawnPoint(string itemId) {
        return spawningPoints[itemId];
    }

    public void AddPosition(string itemId, int spawnPointId) {
        this.spawningPoints.Add(itemId, spawnPointId);
    }

}
