using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceLights : MonoBehaviour
{
    private bool isActive = false;
    public float timeDelay;
    public bool enabledLight = false;
    
    // Update is called once per frame
    void Update()
    {
        if(!isActive) {
            StartCoroutine(ActiveLight());
        }
    }

    IEnumerator ActiveLight() {
        isActive = true;
        enabledLight = !enabledLight;
        this.gameObject.GetComponent<Light>().enabled = enabledLight;
        yield return new WaitForSeconds(timeDelay);
        enabledLight = !enabledLight;
        this.gameObject.GetComponent<Light>().enabled = enabledLight;
        yield return new WaitForSeconds(timeDelay);
        isActive = false;
    }
}
