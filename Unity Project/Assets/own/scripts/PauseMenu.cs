using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PauseMenu : MonoBehaviour
{
    public GameObject PAUSE_MENU;
    private bool visible;

    // Start is called before the first frame update
    void Start()
    {
        this.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetActive(bool active) {
        if(!active) {
            Time.timeScale = 1;
        } else {
            Time.timeScale = 0;
        }

        PAUSE_MENU.SetActive(active);

        this.visible = active;
    }

    public bool isVisible() {
        return visible;
    }
}
