using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue 
{
    public string name;
    public string gameObjectName;
    public string animationObject; 
    public bool relevant;
    public float startByDistance = 0.0f;
    
    public Sentence[] sentences;
    private List<string> allKeys;
    private bool alreadyMeet = false;

    public void PrepareKeyWords(string color) {
        allKeys = new List<string>();
        foreach (Sentence sentence in sentences) 
        { 
            string[] sentenceKeys = sentence.keys.Split(';');
            allKeys.AddRange(sentenceKeys);
        }

        for (int index = 0; index < sentences.Length; index++)
        {
          foreach (string key in allKeys) 
            {
                if(key != "")
                    sentences[index].text = sentences[index].text.Replace(key, "<color=#" + color + ">" + key + "</color>");
            } 
        }
    }

    
    public bool GetAlreadyMeet() {
        return this.alreadyMeet;
    }

    public void SetAlreadyMeet(bool alreadyMeetNew) {
        this.alreadyMeet = alreadyMeetNew;
    }
}
