using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using StarterAssets;

public class SceneSwitch : InteractionObjects
{

    private GameObject playerObject;
    public string firstScene, secondScene, thirdScene;
    public float interactiveDistance = 60f;
    public string[] requiredItemsScene1;
    private int positionSet = 0;
    public AudioClip sfx;

    // Start is called before the first frame update
    void Start()
    {
        playerObject = GameObject.Find("PlayerArmature");
        DontDestroyOnLoad(GameObject.Find("Player3D"));
        GameObject.Find("GameStateText").GetComponent<GameState>().SetState(0);
    }

    public void SetPosition() {
        GameObject playerSpawn = GameObject.Find("PlayerSpawn");
        if(playerSpawn is not null && this.positionSet < 3) {
            this.playerObject.GetComponent<Transform>().position = playerSpawn.GetComponent<Transform>().position;
            this.positionSet++;
        }
    }

    void Update() {
        
        SetPosition();

        int[] leavingMessageStates = {2, 4, 5, 6};
        int[] interactionStates = {1, 2, 3};

        float distance = Vector3.Distance(transform.position, playerObject.GetComponent<Transform>().position);

        if(distance > interactiveDistance && Array.IndexOf(leavingMessageStates, GameObject.Find("GameStateText").GetComponent<GameState>().GetState()) != -1) {
            GameObject.Find("GameStateText").GetComponent<GameState>().SetState(0);
        }

        if(distance < interactiveDistance && GetInteractable()) {
            if(SceneManager.GetActiveScene().name == firstScene) {
                // leave flat
                GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(1);
            } else if(SceneManager.GetActiveScene().name == secondScene) {
                // leave park
                GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(2);
            } else {
                // leave police station
                GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(3);
            }

            this.lastInteractedObject = true;
        } else if(this.lastInteractedObject && Array.IndexOf(interactionStates, GameObject.Find("InteractionText").GetComponent<InteractionMessage>().GetMessageType()) != -1) {
            // clear message
            GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(-1);

            this.lastInteractedObject = false;
        }
    }

    /* // Start is called before the first frame update
    void onAwake()
    {
        GameObject playerSpawn = GameObject.Find("PlayerSpawn");
        if(playerSpawn) {
            playerObject.GetComponent<Transform>().position = playerSpawn.GetComponent<Transform>().position;
        }
    } */

    // Update is called once per frame
    public override void OnInteract()
    {
        bool proceed = true;

        if (sfx != null)
                AudioSource.PlayClipAtPoint(sfx, transform.position, 10.0f);

        if(SceneManager.GetActiveScene().name == firstScene) {
            
            bool badge = false;
            bool gun = false;
            bool light = false;

            List<InventoryItem> itemsInInventory = GameObject.Find("Inventory").GetComponent<Inventory>().GetItems();
            int requiredItemsInInventory = 0;
            foreach(InventoryItem itemInventory in itemsInInventory) {
                if(itemInventory.id == "gun") {
                   gun = true;
                }

                if(itemInventory.id == "badge") {
                   badge = true;
                }

                if(itemInventory.id == "flashLight") {
                   light = true;
                }

                if(Array.IndexOf(requiredItemsScene1, itemInventory.id) != -1) {
                    requiredItemsInInventory++;
                }
            }

            if(requiredItemsInInventory != requiredItemsScene1.Length) {
                proceed = false;
                if(!badge) {
                    GameObject.Find("GameStateText").GetComponent<GameState>().SetState(4);
                } else if(!gun) {
                    GameObject.Find("GameStateText").GetComponent<GameState>().SetState(6);
                } else if(!light) {
                    GameObject.Find("GameStateText").GetComponent<GameState>().SetState(5);
                } else {
                    GameObject.Find("GameStateText").GetComponent<GameState>().SetState(2);
                }
            }
        }


        float distance = Vector3.Distance(transform.position, playerObject.GetComponent<Transform>().position);
        // if input code for use object is down and palyer is near
        if(!proceed) {

        } else {
            if(distance < interactiveDistance) {

                GameObject.Find("GameStateText").GetComponent<GameState>().SetState(900);

                string nextScene = "";
                // if player is in scene 1, he can go to scene 2
                if(SceneManager.GetActiveScene().name == firstScene) {
                    nextScene = secondScene;
                } else {
                    // if player is in scene 2, he can go to scene 3 and conversely
                    if(SceneManager.GetActiveScene().name == secondScene) {
                        nextScene = thirdScene;
                    } else if(SceneManager.GetActiveScene().name == thirdScene) {
                        nextScene = secondScene;
                    }
                }
                SceneManager.LoadScene(nextScene);
            }
        }
    }
}
