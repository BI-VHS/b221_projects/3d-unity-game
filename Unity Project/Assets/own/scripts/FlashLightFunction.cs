using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLightFunction : MonoBehaviour
{
    public bool lightOn = false;
    public AudioClip[] sfx;

    private void Start()
    {
        GameObject.Find("SpotlightFlashLight").GetComponent<Light>().enabled = lightOn;
    }

    public void Switch() {

        if (GameObject.Find("Inventory").GetComponent<Inventory>().IsInInventory("flashLight")) {
            AudioSource.PlayClipAtPoint(sfx[lightOn ? 1 : 0], transform.position);
            lightOn = !lightOn;
            Animator animFlashLight = this.gameObject.GetComponent<Animator>();
            animFlashLight.SetBool("pullUp", lightOn);
            GameObject.Find("SpotlightFlashLight").GetComponent<Light>().enabled = lightOn;
        } else {
            Debug.Log("You haven't flashlight!");
        }  
    }
}