using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sentence 
{   
    public string description;
    public bool used = false;
    public bool repeatable = false;
    public bool systemText = false;
    public int animationState = 0;
    public int logicalState = 0;
    public int useCounter = 0;
    public string logicalScriptObject;
    public string neededEvidence;
    [TextArea(2,10)]
    public string keys;
    [TextArea(3,10)]
    public string playerText;
    [TextArea(3,10)]
    public string text;
    [TextArea(3,10)]
    public string usedText;
    
}