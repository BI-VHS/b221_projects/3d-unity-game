using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscussNPC : MonoBehaviourChild
{
    public float interactiveDistance = 60f;
    private GameObject playerObject;
    private float distance;
    private DialogueManager dialogueManager;

    void Start() {
        dialogueManager = FindObjectOfType<DialogueManager>();
        dialogueManager.SetInDiscussWith(gameObject.name);
        playerObject = GameObject.Find("PlayerArmature");

        GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(-1);
    }

    void Update() {
        toggleInteractionMessage(4);
    }
}
