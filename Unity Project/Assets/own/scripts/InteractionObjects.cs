using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionObjects : MonoBehaviourChild
{    
    private BoxCollider thisCollider;


    // Start is called before the first frame update
    void Start()
    {
        // get this object collider
        thisCollider = this.gameObject.GetComponent<BoxCollider>();
    }

    virtual public void OnInteract() {}
}
