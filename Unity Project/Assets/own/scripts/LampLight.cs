using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampLight : MonoBehaviour
{
    
    public bool enabledLight = false;
    public Material lampMaterial_ON;
    public Material lampMaterial_OFF;

    private bool isActive = false;
    private float timeDelay;
    private MeshRenderer renderer;
    private Material[] materials; 
    
    void Start() {
        renderer = GameObject.Find("street_lamp_3_broken").GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

        if(!isActive) {
            StartCoroutine(ActiveLight());
        }
    }

    IEnumerator ActiveLight() {

        timeDelay = Random.Range(1.0f, 10.0f) / 10.0f;
        isActive = true;
        enabledLight = !enabledLight;
        this.gameObject.GetComponent<Light>().enabled = enabledLight;
        ChangeLampMaterial();
        yield return new WaitForSeconds(timeDelay);
        enabledLight = !enabledLight;
        this.gameObject.GetComponent<Light>().enabled = enabledLight;
        ChangeLampMaterial();
        yield return new WaitForSeconds(timeDelay);
        isActive = false;
    }

    private void ChangeLampMaterial() {
        materials = renderer.materials;
        materials[1] = (enabledLight ? lampMaterial_ON : lampMaterial_OFF);
        renderer.materials = materials;
    }
}
