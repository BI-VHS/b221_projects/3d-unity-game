using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionItem : InteractionObjects
{
    [SerializeField] private InventoryItem item;
    public AudioClip sfx;


    public override void OnInteract()
    {
        // clear message
        GameObject.Find("InteractionText").GetComponent<InteractionMessage>().SetMessageType(-1);

        GameObject.Find("Inventory").GetComponent<Inventory>().AddItem(item);
        Destroy(gameObject);
        if (sfx != null)
            AudioSource.PlayClipAtPoint(sfx, transform.position, 10.0f);
    }

    public string GetItemId() {
        return item.id;
    }

    void Update() {
        toggleInteractionMessage(5);
    }
}
