using UnityEngine;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
#endif

namespace StarterAssets
{
	public class StarterAssetsInputs : MonoBehaviour
	{
		[Header("Character Input Values")]
		public Vector2 move;
		public Vector2 look;
		public bool jump;
		public bool sprint;
		public bool interact;
		public bool escape;
		public bool confirm;
		public bool flashLight;
		public int cameraMode;
		public float[,] cameraCoordinates = {{51.01f,6.98f,0f,12.76f}, {51.01f,6.98f,0f,-1.2f}};

		[Header("Movement Settings")]
		public bool analogMovement;

		[Header("Mouse Cursor Settings")]
		public bool cursorLocked = true;
		public bool cursorInputForLook = true;

#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
		public void OnMove(InputValue value)
		{
			MoveInput(value.Get<Vector2>());
		}

		public void OnLook(InputValue value)
		{
			if(cursorInputForLook)
			{
				LookInput(value.Get<Vector2>());
			}
		}

		public void OnJump(InputValue value)
		{
			JumpInput(value.isPressed);
		}

		public void OnInteract(InputValue value)
		{
			InteractInput(value.isPressed);
		}

		public void OnEscape(InputValue value)
		{
			EscapeInput(value.isPressed);
		}

		public void OnConfirm(InputValue value)
		{
			ConfirmInput(value.isPressed);
		}

		public void OnFlashLight(InputValue value)
		{
			FlashLightInput(value.isPressed);
		}

		public void OnCamera(InputValue value)
		{
			
			if(value.isPressed) {
				cameraMode ++; 
			}
			if(cameraMode == cameraCoordinates.Length) {
				cameraMode = 0;
			}

			CameraInput(cameraMode);
		}

		public void OnSprint(InputValue value)
		{
			SprintInput(value.isPressed);
		}
#endif


		public void MoveInput(Vector2 newMoveDirection)
		{
			move = newMoveDirection;
		} 

		public void LookInput(Vector2 newLookDirection)
		{
			look = newLookDirection;
		}

		public void JumpInput(bool newJumpState)
		{
			jump = newJumpState;
		}

		public void CameraInput(int newCameraMode)
		{
			cameraMode = newCameraMode;
		}

		public void InteractInput(bool newInteractState)
		{
			interact = newInteractState;
		}
		
		public void EscapeInput(bool newEscapeState)
		{
			escape = newEscapeState;
		}

		public void ConfirmInput(bool newConfirmSate)
		{
			confirm = newConfirmSate;
		}

		public void FlashLightInput(bool newState)
		{
			flashLight = newState;
		}

		public void SprintInput(bool newSprintState)
		{
			sprint = newSprintState;
		}

		private void OnApplicationFocus(bool hasFocus)
		{
			SetCursorState(cursorLocked);
		}

		private void SetCursorState(bool newState)
		{
			Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
		}
	}
	
}