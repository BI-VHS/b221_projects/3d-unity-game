# Scény

## Byt detektiva
Zde samotná hra začíná. Tato scéna bude pojata jakožto tutoriál pro samotnou hru.

Scéna je tvořena:
* Místnost 1+kk
* Zavřená koupelna (další potencionální místnost)

![Byt](/Documentation/imgs/Byt.png "Byt detektiva")

![Byt1](/Documentation/imgs/Byt1.png "Byt detektiva ukázka ze hry")

Byt obsahuje vše nezbytné pro přežití singl mládence, jakým je náš věhlasný detektiv. Nepotřebuje 
více nežli je potřeba pro přespání, zahnání nudy u televize, ohřátí řízků z bistra, nebo k vychlazení 
„lahváčů“.

Po bytě budou poschovávané věci, které bude náš detektiv potřebovat najít k výkonu práce = tutoriál 
k ovládání hry a naučení mechanik

## Park – místo činu

![park mapa](/Documentation/imgs/park-mapa.png "Park mapa")

![park ukázka ze hry](/Documentation/imgs/park.png "Park ukázka ze hry")

Park je jedna ze stěžejních scén hry. Zde byl spáchána vražda a jakožto detektiv zde musíme najít 
stopy, které nám tento případ pomohou vyřešit.
Objekty, které budou představovat stopy, se budou náhodně spawnovat dle určených placeholderů a 
dle náhodně zvoleného pachatele.
Na tvorbu parku byly použity jak vlastní modely, tak i assety z Unity asset storu a jsou použity tak, aby 
dotvořili celkovou atmosféru. Taktéž je zde použito již několik dynamických prkvů, jako jsou například particle systémy, animace postav stažených assetů, přepínání světel atd.
Ze scény bude hráč mít možnost přejít do scény policejní stanice

## Policejní stanice:

Policejní stanice je tvořena třemi místnostmi (zleva na obrázku):
* Výslechová místnost
* Chodba
* Kancelář detektiva

![park ukázka ze hry](/Documentation/imgs/policejni_stanice1.JPG "Policejní stanice ")

Design policejní stanice je moderní, s minimem nepotřebných objektů, které by hráče rozptylovaly od 
řešení případu.

### Výslechová místnost:

Výslechová místnost má po obvodu dvoje dveře, jedny vedou do chodby s kanceláří našeho detektiva 
a druhé k celám s podezřelými, a okno z polopropustného zrcadla do vedlejší místnosti. Uprostřed 
místnosti je stůl se židlemi, který natáčí dvě kamery umístěné naproti sobě v rozích místnosti.

### Chodba:

Chodba je prázdná, kromě skříně mezi kancelářemi. Je lemovaná dveřmi a na jedné stěně je 
nástěnka.

### Kancelář detektiva:

Na jedné straně je vybavena dvěmi tabulemi pro řešení případu. Na jedné z nich budou vyplněny 
informace o případu, druhá bude ponechána hráči na poznámky.
Na druhé straně se nachází stůl s počítačem, židle a skříňka. Pro pohodlí detektiva je kancelář ještě 
vybavena věšákem na oblečení, lampou u stolu a odpadkovým košem.

## Závěr

Každá scéna byla malinko upravena vzhledem k návrhům, tak aby byla více atraktivní pro hráče a měl 
dostatek místa pro pohyb ve scéně. Dále najdete tabulku použitých modelů.



