## DETEKTIV - KOLEGA

1. Jméno: Václav Polívka
2. Věk: 48
3. Zaměstnání: Detektiv PČR
4. Rodina: žije dlouhodobě sám.
5. Vztah k hráčovi: kolega
6. Vztah k Oběti: 
7. Žádný
8. Bratr oběti sloužil jako prostředník při prodeji drog

### Možné stopy:

• Otisk boty vel. 46
• Propiska z práce.
• Kus kabátu na křoví
• Tel. číslo v mobilu oběti
• Balíček drog z policejního skladu.

### Background:

Už od mala Václav inklinoval k tomu, aby byl detektiv, jako jeho otec, který byl jeho vzorem. Jakmile 
to bylo možné šel na policejní akademii a dal si za cíl být čestným policistou a ochraňovat lidi ve svém 
městě. Na okrsku, měl vždy výborné výsledky a pouze pár nedořešených případů.
Konec jako vrah:
Jelikož celý život vzhlížel k otci a pak zjistil, že jeho otec byl zkorumpovaný polda, který kryl 
kriminálníky a to poté stálo život, jak jeho otce, tak i jeho matku.
Po tom to zjištění, se Václavovi zhroutil celý svět a sám se vydal na šikmou plochu zločinu prodejem 
drog, které kradl ze skladu. Bratr oběti sloužil jako prostředník při prodeji drog, když se rozhodl 
skončit s touto prací, tak ho Václav vylákal ven a zabil.
Oběť (sestra) to později zjistila a začala tlačit na detektiva, tak jí taky zabil, aby se zbavil i jí a 
veškerého podezření.

## BRATR

1. Jméno: Petr Kratochvíl
2. Věk: 21, Má se za mrtvého
3. Zaměstnání: Student
4. Rodina: Setra (oběť), matka.
5. Vztah k hráčovi: none
6. Vztah k Oběti: Bratr

### Možné stopy:

• Peněženka
• Přívěšek od klíčů
• Parkovací lístek od školy.
• Otisk boty vel. 43

### Background:

Od mala byl Petr zvídavý chlapec a bohužel se vždy namočil do nějakého maléru. Na střední a vysoké 
škole se zdálo, že se již zklidnil a vše je v pořádku až do doby kdy tragicky zemřel, když se opil a spadl 
z mostu do řeky. Jeho tělo bohužel nebylo nikdy nalezeno a jeho sestra se s tímto faktem nikdy 
nesmířila. Od zmizení bratra hledala stopy k tomu, aby ho nalezla, jak sama věřila živého.
Konec jako Vrah:
Ano bratr přežil. Věděl, že jeho sestra zjistila o prodeji drog a proto nevěděl, jak toto řešit, aniž by jí 
ublížil. Nakonec se rozhodl, že na finguje svou smrt a málem mu to vyšlo, nebýt jeho sestry, která ho 
stejně vypátrala. Bohužel v té době, měl Petr už zničenou mysl užíváním návykových látek a také 
věděl, že kdyby na něj někdo přišel, tak i na jeho šéfa a tím by ohrozil i jeho matku. Se sestrou se 
sešel, aby jí to rozmluvil, vysvětlil situaci a potenciální nebezpečí. Bohužel se to vše zvrtlo a skončilo 
smrtí sestry.

## MATKA

1. Jméno: Alena Kratochvílová
2. Věk: 43
3. Zaměstnání: Pomocná síla v supermarketu
4. Rodina: dcera, syn (mrtvý)
5. Vztah k hráčovi: none
6. Vztah k Oběti: matka

### Možné stopy:

• Svědkyně slyšela, jak se dvě ženy hádají
• Otisk boty vel. 39
• matka u výslechu roztěkaná + obvázané levé zápěstí
• cizí krev na místě (jak se chtěla podřezat)

### Background:

Alenu zbouchnul v prváku na vysoké spolužák. Předpokládala, že spolu založí rodinu, ten ji ovšem 
ještě před porodem opustil. Musela zanechat vysoké školy, nikdy neměla žádnou kariéru a živila se 
hůře placenými prácemi. Stejně tak si již nikdy jako svobodná matka nenašla vhodného partnera, se 
kterým by chtěla dlouhodobě žít. To vše dávala za vinu své dceři a jejímu otci.
Konec jako Vrah:
Alena věděla, že dcera půjde do parku. Zmínila se před mámou, že se tam má sejít s kamarádkou. Šla 
tam taky, protože si myslela, že se nesejde s kamarádkou, ale s nějakým klukem, což ji chtěla 
rozmluvit. Aby neudělala stejnou chybu (oběti bylo 23 let). Když dcera čekala v parku, matka za ní 
přišla a začala ji domlouvat. To vše vyústilo v hádku, kdy Alena vyčetla vše své dceři a v záchvatu ji 
zabila. Pak si chtěla sama sáhnout na život, ale neměla na to nakonec odvahu.

## SVĚDKYNĚ

1. Jméno: Monika Šimková
2. Věk: 23
3. Zaměstnání: Studentka
4. Rodina: -
5. Vztah k hráčovi: none
6. Vztah k Oběti: nejlepší kamarádka

### Možné stopy:

• Otisk boty vel. 37
• vytrhané vlasy na místě činu (Anety i Moniky - může to být třeba blondýna)
• Anetě chyběl řetízek, který dostala od Adama a stále ho nosila (to nám poví matka)
• SMSky v telefonu Anety od Moniky, s žárlivým textem ve stylu, že Adam pro ní není ten 
pravý…

### Background:

Nejlepší kamarádka Anety. Poznaly se na vysoké škole a padly si hned do noty. Jednou se jim oběma 
zalíbil Adam - starší student. Adamovi se ale zalíbila Aneta a tak si s ní začal. Monika žárlila…
Konec jako Vrah:
Monika věděla, že se Aneta měla s někým sejít v parku a myslela si, že jde o Adama. Ve skutečnosti 
šlo o inspektora (kolegu), který s ní chtěl probrat nové údaje okolo jejího bratra. S Anetou se 
pohádala, myslela si, že jí zapírá, když říkala, že Adam nepřijde a v afektu na ní zaútočila (asi by 
neměla být vražedná zbraň kuchyňský nůž, ale nějaký kapesní). Když si uvědomila co udělala, myslela 
si, že nejlepší krytí bude zavolat policii a tvrdit, že byla svědkem.
