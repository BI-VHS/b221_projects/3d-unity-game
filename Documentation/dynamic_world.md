# Animace

## Policisté

Hotové animace:

* IDLE (pohupování se)
* Chůze
* Souhlas - hotové v unity, ne v buildu
* Nesouhlas - hotové v unity, ne v buildu
* Základní komunikace.*
* Hledání v parku: dvě verze. Jedna neimplementována v unity => problémy s rozdílným přístupek k rigování.

Jeden, nebo více policistů budou chodit po parku a hlídkovat případně se snažit najít důkazní materiály.

![policie idle](/Documentation/imgs/policie_idle.gif "Policie idle")
![policie hledání](/Documentation/imgs/policie_search2.gif "Policie hledání")
![policie chůze](/Documentation/imgs/policie_walk.gif "Policie chůze")
![policie ano](/Documentation/imgs/policie_yes.gif "Policie ano")
![policie ne](/Documentation/imgs/policie_no.gif "Policie ne")
![policie neimplementováno](/Documentation/imgs/policie_search.gif "Policie hledání")

## Svědkyně

Hotové animace:

* IDLE (nervózní pohybování sem a tam)
* ukazování na místo kde může být schovaný důkazní předmět 
* základní komunikace.*
   * Podezřelá verze. (Tikání očí když bude lhát)
   * Normální verze.

![svědkyně idle](/Documentation/imgs/sophie_idle.gif "Svědkyně idle")
![svědkyně ukazování na místo](/Documentation/imgs/sophie_pointing.gif "Svědkyně ukazování na místo")
![svědkyně základní komunikace](/Documentation/imgs/sophie_talking.gif "Svědkyně základní komunikace")

## Matka

Hotové animace:
* IDLE ("pohyb" na židli) 
   * Zakládání hlavy do dlaní.
   * Složení rukou do sebe a přemýšlení.
* základní komunikace.*

![Matka komunikace](/Documentation/imgs/mother_talk.gif "Matka komunikace")
![Matka přemýšlení](/Documentation/imgs/mother_thinking.gif "Matka přemýšlení")
![Matka zakládání hlavy do dlaní](/Documentation/imgs/mother_no_way.gif "Matka zakládání hlavy do dlaní")

## Kolega

Hotové animace:

Dají se použít ty samé animace, jako u policistů

* IDLE "pohupování se"
* chůze
* základní komunikace.*
* atd.

## Rostlinka v akvárku

Hotové animace:
* IDLE (pohyb sem a tam) 

![plant idle](/Documentation/imgs/aquario_plant.gif "Rostlinka idle")

## Rybka v akvárku

Hotové animace:
* IDLE (plavání sem a tam) 
* Když hráč bude brát předmět z akvária, tak se rybka vyleká a schová se za rostlinku.

![rybka idle](/Documentation/imgs/fish_idle.gif "Rybka idle")
![rybka leknutí](/Documentation/imgs/fish_scared.gif "Rybka leknutí")
![rybka schovávání](/Documentation/imgs/fish_hiding.gif "Rybka schovávání")

V plánu:
* Hráč bude mít dostupné krmivo pro akvarijní rybičky. Po nasypání do akvárka začne rybka pojídat krmivo.Takto půjde rybka vylákat po leknutí se hráče. - neimplementováno

## Světlušky v parku

Hotové animace pomocí particle:
* IDLE

V plánu:
* Při přiblížení hráče se pomalu vytratí.

![světlušky idle](/Documentation/imgs/fireflyes.gif "Světlušky idle")

## Fontána v parku

Hotové animace pomocí particle:
* IDLE

![fontána idle](/Documentation/imgs/fountain.gif "Fontána idle")

## Veverka v parku

Hotové animace:
* IDLE
* Běh
* Pojídání oříšku
* Čmuchání a hledání oříšků (původně mělo být velezení na strom před hráčem nakonec neimplementováno)

![veverka idle](/Documentation/imgs/sq_idling.gif "Veverka idle")
![veverka běh](/Documentation/imgs/sq_run.gif "Veverka běh")
![veverka pojídání](/Documentation/imgs/sq_eating.gif "Veverka pojídání")
![veverka čmuchání a hledání oříšků](/Documentation/imgs/sq_holding_tree.gif "Veverka čmuchání a hledání oříšků")


# Produkce:

## Veverka
* Veverka běhá mezi místy v přírodě, kde mohou být umístěny důkazní materiály a na těchto místech zanechává oříšky.
* Taktéž slouží jako možné vodítko pro hráče

![veverka ořechy](/Documentation/imgs/nuts.gif "Veverka ořechy")

## Kolega

V plánu:
Když přijde do kanceláře kolega tak tak vždy hodí kelímek od kafe do koše. Jakmile bude počet kelímků nad určitý počet, tak kolega začne říkat mezi konverzací věty typu "měl bys ty koše vyhazovat častěji." "Už by sis měl vynést ten koš", atd.

## Policista

Hotová animace:
Jeden policista bude sem tam doplňovat šanony do skříně na chodbě policejní stanice. (neimplementováno v Unity. Stejný problém s rigem jako u verze hledání)

# Unikáty

V plánu:
* policista chodící po parku si bude stěžovat na práci. (Animace jsou hotové, bude potřeba připravit audio stopu a animaci upravit dle scénáře)
* policista na stanici u výslechové místnosti bude kooperovat s hráčem a vodit mu do výslechové místnosti podezřelé. - v buildu pouze říká, že je tam již matka
