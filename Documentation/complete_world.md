# Úkoly

Inspirací byla phasmophobia a klasické unikové hry, kde není vždy zcela nezbytné vyřešit všechny hádanky, ale stačí nalézt pouze to s čím si hráč vystačí na správné vyřešení případu

* dostat se z bytu a osvojit si ovládání hry
* nalézt důkazy (nemusí všechny)
* provést výslechy (nemusí všechny)
* dopadnout vraha (když si je jistý)

# Naprogramované funkce

* interakce s objekty
   * důkazy a věci detektiva - dají se sebrat do iventáře
   * vyplnění stopy odlitkem
   * odstranní závěsu, otvírání dvěří, otvírání košů
   * funkce baterky (pouze když má hráč baterku v inventáři)
   * nápověda ovládání + hint při míření na interaktivní prvek
   * přesun mezi scénami

![koš](/Documentation/imgs/bin.gif "Koš")

* objekty, které mají činnost
   * hlídkující policista
   * přepínání světel vozu policie
   * náhodně blikající lampa
   * přesun veverky mezi místy, kde se může nacházet důkaz a vyhazování oříšků
   * doplňování textu na whiteboard
   * příjem emailu na PC detektiva

![hlídkující policista](/Documentation/imgs/policie_search2_torch.gif "Hlídkující policista")
![oříšky](/Documentation/imgs/nuts.gif "Oříšky")

* komunikace s NPC
   * vlastní dialogový systém pomocí psaných otázek od hráče (implementováno pomocí klíčových slov)
   * automatické označování klíčových slov
   * reakce na důkazy, které jsou v inventáři
   * spouštění animací pomocí postupu v konverzaci
   * možnost opakování některých vět a některých ne.
   * spouštění scriptů na základěpostupu v konverzaci
   * některá klíčová slova jsou skryta a hráč je může objevit pouze tím, že je zkusí ašak když bude koukat pozorna nápovědu najde
   * vše předešlé lze nastavovat pomocí našeho manageru v unity bez nutnosti programování

![ukázka konverzace](/Documentation/imgs/conversation.gif "Ukázka konverzace")

# Zvuky

v kompletním světě byly doplněny zvuky pro různé typy interakcí a pro ambient:

* ambient 
   * v parku
   * na stanici
   * zvuk fontány
   * pískání veverky

* interakce
   * baterka
   * sebrání předmětu (různé zvuky pro předměty)
   * vypínače
   * závěsy
   * startování auta
   * dveře
   * zvuk výka od koše v parku
